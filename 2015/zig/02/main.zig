const std = @import("std");

const input = @embedFile("input");

pub fn main() !void {
    std.debug.print("part 1: {}\n", .{part1()});
    std.debug.print("part 2: {}\n", .{part2()});
}

fn part1() usize {
    var in_it = std.mem.tokenize(u8, input, "\n");

    var feet: usize = 0;
    while (in_it.next()) |row| {
        var line_it = std.mem.tokenize(u8, row, "x");
        var lwh: [3]usize = undefined;
        var i: u32 = 0;
        while (line_it.next()) |e| : (i += 1) {
            lwh[i] = std.fmt.parseUnsigned(usize, e, 10) catch unreachable;
        }

        std.sort.sort(usize, &lwh, {}, comptime std.sort.asc(usize));
        feet += (2 * lwh[0] * lwh[1]) + (2 * lwh[1] * lwh[2]) + (2 * lwh[2] * lwh[0]) + (lwh[0] * lwh[1]);
    }
    return feet;
}

fn part2() usize {
    var in_it = std.mem.tokenize(u8, input, "\n");

    var ribbon: usize = 0;
    while (in_it.next()) |row| {
        var line_it = std.mem.tokenize(u8, row, "x");
        var lwh: [3]usize = undefined;
        var i: u32 = 0;
        while (line_it.next()) |e| : (i += 1) {
            lwh[i] = std.fmt.parseUnsigned(usize, e, 10) catch unreachable;
        }

        std.sort.sort(usize, &lwh, {}, comptime std.sort.asc(usize));
        ribbon += (lwh[0] + lwh[0] + lwh[1] + lwh[1]) + lwh[0] * lwh[1] * lwh[2];
    }
    return ribbon;
}
