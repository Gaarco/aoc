const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    const target = b.standardTargetOptions(.{});
    const mode = b.standardReleaseOptions();

    comptime var day: u32 = 1;
    inline while (day <= 25) : (day += 1) {
        const day_num = comptime std.fmt.comptimePrint("{d:0>2}", .{day});

        const exe = b.addExecutable("day" ++ day_num, day_num ++ "/main.zig");
        exe.setTarget(target);
        exe.setBuildMode(mode);
        exe.install();

        const run_cmd = exe.run();
        run_cmd.step.dependOn(b.getInstallStep());
        if (b.args) |args| {
            run_cmd.addArgs(args);
        }

        const run_step = b.step("day" ++ day_num, "Run day number " ++ day_num);
        run_step.dependOn(&run_cmd.step);
    }
}
