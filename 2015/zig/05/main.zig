const std = @import("std");

const input = @embedFile("input");

pub fn main() !void {
    std.debug.print("Part 1: {d}\n", .{part1()});
    std.debug.print("Part 2: {d}\n", .{part2()});
}

fn part1() u32 {
    const vowels = "aeiou";
    const bad_str = [_][]const u8{ "ab", "cd", "pq", "xy" };
    var it = std.mem.tokenize(u8, input, "\n");

    var nice: u32 = 0;
    outer: while (it.next()) |r| {
        for (bad_str) |s| {
            if (std.mem.count(u8, r, s) > 0) continue :outer;
        }

        var last: ?u8 = null;
        var twice: bool = false;
        var vowc: u32 = 0;

        for (r) |c| {
            for (vowels) |v| {
                if (c == v) vowc += 1;
            }
            if (last) |l| {
                if (l == c) twice = true;
            }
            last = c;
        }

        if (vowc >= 3 and twice) {
            nice += 1;
        }
    }

    return nice;
}

fn part2() u32 {
    var it = std.mem.tokenize(u8, input, "\n");

    var nice: u32 = 0;
    while (it.next()) |r| {
        var repeat_found: bool = false;
        var pair_found: bool = false;

        var i: u32 = 0;
        while (i < r.len) : (i += 1) {
            if (i < r.len - 2 and !repeat_found) {
                repeat_found = r[i] == r[i + 2];
            }
            if (i < r.len - 1 and !pair_found) {
                const needle = [2]u8{ r[i], r[i + 1] };
                pair_found = std.mem.containsAtLeast(u8, r, 2, &needle);
            }
        }

        if (repeat_found and pair_found) {
            nice += 1;
        }
    }

    return nice;
}
