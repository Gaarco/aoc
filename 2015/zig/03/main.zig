const std = @import("std");

const input = @embedFile("input");

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    std.debug.print("part 1: {}\n", .{part1(gpa.allocator())});
    std.debug.print("part 2: {}\n", .{part2(gpa.allocator())});
}

const Pos = struct {
    x: i32,
    y: i32,

    pub fn update(self: *Pos, c: u8) void {
        if (c == '^') self.y += 1;
        if (c == 'v') self.y -= 1;
        if (c == '>') self.x += 1;
        if (c == '<') self.x -= 1;
    }
};

fn part1(allocator: std.mem.Allocator) usize {
    var positions = std.AutoHashMap(Pos, usize).init(allocator);

    var curr_pos = Pos{ .x = 0, .y = 0 };

    for (input) |c| {
        if (positions.getPtr(curr_pos)) |v| {
            v.* += 1;
        } else {
            positions.put(curr_pos, 1) catch unreachable;
        }

        curr_pos.update(c);
    }

    var iter = positions.iterator();
    var count: u32 = 0;
    while (iter.next()) |e| {
        if (e.value_ptr.* >= 1) count += 1;
    }

    return count;
}

fn part2(allocator: std.mem.Allocator) usize {
    var positions = std.AutoHashMap(Pos, usize).init(allocator);

    var santa_pos = Pos{ .x = 0, .y = 0 };
    var robo_pos = Pos{ .x = 0, .y = 0 };

    for (input) |c, i| {
        if (positions.getPtr(santa_pos)) |v| {
            v.* += 1;
        } else {
            positions.put(santa_pos, 1) catch unreachable;
        }

        if (positions.getPtr(robo_pos)) |v| {
            v.* += 1;
        } else {
            positions.put(robo_pos, 1) catch unreachable;
        }

        if (i % 2 == 0) {
            santa_pos.update(c);
        } else {
            robo_pos.update(c);
        }
    }

    var iter = positions.iterator();
    var count: u32 = 0;
    while (iter.next()) |e| {
        if (e.value_ptr.* >= 1) count += 1;
    }

    return count;
}
