const std = @import("std");

const input = @embedFile("input");

pub fn main() !void {
    std.debug.print("Part 1: {}\n", .{part1()});
    std.debug.print("Part 2: {}\n", .{part2()});
}

const toggle = "toggle ";
const turn_on = "turn on ";
const turn_off = "turn off ";

const Action = enum {
    toggle,
    on,
    off,
};

fn part1() u32 {
    var grid = std.mem.zeroes([1000][1000]u1);

    var it = std.mem.tokenize(u8, input, "\n");
    while (it.next()) |r| {
        var action: Action = undefined;
        var str = blk: {
            if (std.mem.containsAtLeast(u8, r, 1, toggle)) {
                action = .toggle;
                break :blk r[toggle.len..];
            } else if (std.mem.containsAtLeast(u8, r, 1, turn_on)) {
                action = .on;
                break :blk r[turn_on.len..];
            } else if (std.mem.containsAtLeast(u8, r, 1, turn_off)) {
                action = .off;
                break :blk r[turn_off.len..];
            } else unreachable;
        };

        var lit = std.mem.tokenize(u8, str, " ");
        const from = lit.next().?;
        var comma_idx = std.mem.indexOf(u8, from, ",").?;
        const x = std.fmt.parseUnsigned(u32, from[0..comma_idx], 10) catch unreachable;
        const y = std.fmt.parseUnsigned(u32, from[comma_idx + 1 ..], 10) catch unreachable;

        _ = lit.next();

        const to = lit.next().?;
        comma_idx = std.mem.indexOf(u8, to, ",").?;
        const to_x = std.fmt.parseUnsigned(u32, to[0..comma_idx], 10) catch unreachable;
        const to_y = std.fmt.parseUnsigned(u32, to[comma_idx + 1 ..], 10) catch unreachable;

        var i: u32 = x;
        while (i <= to_x) : (i += 1) {
            var j: u32 = y;
            while (j <= to_y) : (j += 1) {
                switch (action) {
                    .toggle => grid[i][j] = ~grid[i][j],
                    .on => grid[i][j] = 1,
                    .off => grid[i][j] = 0,
                }
            }
        }
    }

    var count: u32 = 0;
    for (grid) |row| {
        for (row) |e| {
            if (e == 1) count += 1;
        }
    }

    return count;
}

fn part2() u32 {
    var grid = std.mem.zeroes([1000][1000]u32);

    var it = std.mem.tokenize(u8, input, "\n");
    while (it.next()) |r| {
        var action: Action = undefined;
        var str = blk: {
            if (std.mem.containsAtLeast(u8, r, 1, toggle)) {
                action = .toggle;
                break :blk r[toggle.len..];
            } else if (std.mem.containsAtLeast(u8, r, 1, turn_on)) {
                action = .on;
                break :blk r[turn_on.len..];
            } else if (std.mem.containsAtLeast(u8, r, 1, turn_off)) {
                action = .off;
                break :blk r[turn_off.len..];
            } else unreachable;
        };

        var lit = std.mem.tokenize(u8, str, " ");
        const from = lit.next().?;
        var comma_idx = std.mem.indexOf(u8, from, ",").?;
        const x = std.fmt.parseUnsigned(u32, from[0..comma_idx], 10) catch unreachable;
        const y = std.fmt.parseUnsigned(u32, from[comma_idx + 1 ..], 10) catch unreachable;

        _ = lit.next();

        const to = lit.next().?;
        comma_idx = std.mem.indexOf(u8, to, ",").?;
        const to_x = std.fmt.parseUnsigned(u32, to[0..comma_idx], 10) catch unreachable;
        const to_y = std.fmt.parseUnsigned(u32, to[comma_idx + 1 ..], 10) catch unreachable;

        var i: u32 = x;
        while (i <= to_x) : (i += 1) {
            var j: u32 = y;
            while (j <= to_y) : (j += 1) {
                switch (action) {
                    .toggle => grid[i][j] += 2,
                    .on => grid[i][j] += 1,
                    .off => grid[i][j] -|= 1,
                }
            }
        }
    }

    var brightness: u32 = 0;
    for (grid) |row| {
        for (row) |e| {
            brightness += e;
        }
    }

    return brightness;
}
