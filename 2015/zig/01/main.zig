const std = @import("std");

const input = @embedFile("input");

pub fn main() !void {
    std.debug.print("part 1: {}\n", .{part1()});
    std.debug.print("part 2: {}\n", .{part2() orelse unreachable});
}

fn part1() isize {
    var floor: i32 = 0;
    for (input) |c| {
        if (c == '(') floor += 1;
        if (c == ')') floor -= 1;
    }
    return floor;
}

fn part2() ?usize {
    var floor: i32 = 0;
    for (input) |c, i| {
        if (c == '(') floor += 1;
        if (c == ')') floor -= 1;
        if (floor < 0) return i + 1;
    }
    return null;
}
